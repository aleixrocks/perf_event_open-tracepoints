/*
 *  Copyright (C) 2020 Barcelona Supercomputing Center (BSC)
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>
#include <sys/mman.h>
#include <stdint.h>
#include <inttypes.h>
#include <assert.h>
#include <time.h>

#include <dlfcn.h>

/* The "volatile" is due to gcc bugs */
#define io_uring_barrier()	__asm__ __volatile__("": : :"memory")
#define io_uring_mb()		asm volatile("mfence" ::: "memory")
#define io_uring_rmb()		asm volatile("lfence" ::: "memory")
#define io_uring_wmb()		asm volatile("sfence" ::: "memory")
#define io_uring_smp_rmb()	io_uring_barrier()
#define io_uring_smp_wmb()	io_uring_barrier()

#define COUNT_OF(x) (sizeof(x)/sizeof(0[x]))
#define min(a, b) ((a) < (b)) ? (a) : (b)

#define PAGE_SIZE (1<<12)

// cat /sys/kernel/debug/tracing/events/sched/sched_switch/id
#define TP_SCHED_SWITCH 318
#define TP_SCHED_WAKEUP 320

//#define TP_SCHED_SWITCH 316
//#define TP_SCHED_WAKEUP 318

//#define TP_SCHED_SWITCH 275
//#define TP_SCHED_WAKEUP 277

/* perf sample has 16 bits size limit */
#define PERF_SAMPLE_MAX_SIZE (1 << 16)

uint64_t cnt;

struct __attribute__((__packed__)) tp_common {
	unsigned short common_type;
	unsigned char  common_flags;
	unsigned char  common_preempt_count;
	int            common_pid;
};

// cat /sys/kernel/debug/tracing/events/sched/sched_switch/format
struct __attribute__((__packed__)) tp_sched_switch {
	struct tp_common tpc;
	char  prev_comm[16];
	pid_t prev_pid;
	int   prev_prio;
	long  prev_state;
	char  next_comm[16];
	pid_t next_pid;
	int   next_prio;
};

struct __attribute__((__packed__)) tp_sched_wakeup {
	struct tp_common tpc;
	char  comm[16];
	pid_t pid;
	int   prio;
	int   success;
	int   target_cpu;
};

struct perf_record_sample {
	struct perf_event_header header;
	uint64_t time;   // if PERF_SAMPLE_TIME
	uint32_t size;   // if PERF_SAMPLE_RAW
	char     data[]; // if PERF_SAMPLE_RAW
};

struct perf_lost_record {
	struct perf_event_header header;
	uint64_t id;
	uint64_t lost;
	// struct sample_id sample_id;
};

long perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                int cpu, int group_fd, unsigned long flags)
{
	int ret;
	ret = syscall(__NR_perf_event_open, hw_event, pid, cpu,
		      group_fd, flags);
	return ret;
}

#define PERF_HEADER_ENTRY(name)  \
case name: \
	printf(#name " "); \
	break;

void perf_print_header_type(struct perf_event_header *header)
{
	printf(" - entry: ");
	switch (header->type) {
	PERF_HEADER_ENTRY(PERF_RECORD_MMAP)
	PERF_HEADER_ENTRY(PERF_RECORD_LOST)
	PERF_HEADER_ENTRY(PERF_RECORD_COMM)
	PERF_HEADER_ENTRY(PERF_RECORD_EXIT)
	PERF_HEADER_ENTRY(PERF_RECORD_THROTTLE)
	PERF_HEADER_ENTRY(PERF_RECORD_UNTHROTTLE)
	PERF_HEADER_ENTRY(PERF_RECORD_FORK)
	PERF_HEADER_ENTRY(PERF_RECORD_READ)
	PERF_HEADER_ENTRY(PERF_RECORD_SAMPLE)
	default:
		printf("[type %d not known]", header->type);
	}

	switch (header->misc & PERF_RECORD_MISC_CPUMODE_MASK) {
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_CPUMODE_UNKNOWN)
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_KERNEL)
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_USER)
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_HYPERVISOR)
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_GUEST_KERNEL)
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_GUEST_USER)
	PERF_HEADER_ENTRY(PERF_RECORD_MISC_MMAP_DATA)
	default:
		printf("[misc %d not known]", header->misc);
	}

	if (header->misc & PERF_RECORD_MISC_MMAP_DATA)
		printf("PERF_RECORD_MISC_MMAP_DATA");
	else if (header->misc & PERF_RECORD_MISC_EXACT_IP)
		printf("PERF_RECORD_MISC_EXACT_IP");
	else if (header->misc & PERF_RECORD_MISC_EXT_RESERVED)
		printf("PERF_RECORD_MISC_EXT_RESERVED");

	printf("\n");
	printf(" - header size: %"PRIu16"\n", header->size);
}

void check_record_sample_size(struct perf_record_sample *prs)
{
	size_t total, record_extra;
	size_t unknown;

	total = prs->header.size;
	record_extra = sizeof(uint64_t) /*time*/ + sizeof(uint32_t); /*size*/

	unknown = total - sizeof(struct perf_event_header)
			- record_extra
			- prs->size;

	if (unknown) {
		fprintf(stderr,
			"WARNING: sample record unknown bytes: %zu\n"
			" header.size  = %zu\n"
			" size         = %zu\n"
			" record extra = %zu\n",
			unknown, prs->header.size, prs->size, record_extra);
		perf_print_header_type(&prs->header);
		fprintf(stderr, "event count=%"PRIu64" bytes=%lu pages=%f\n", cnt,
			cnt*prs->header.size,
			(float)(cnt*prs->header.size)/4096.0);
	}
}

void check_tp_record_size(struct perf_record_sample *prs, char *name, size_t expected_fields_size)
{
	ssize_t total, record_extra;
	ssize_t unknown;
	ssize_t unknown_fields;
	unsigned char align;
	unsigned char padding;

	total = prs->header.size;
	record_extra = sizeof(uint64_t) /*time*/ + sizeof(uint32_t); /*size*/

	// data is 64 bit aligned, padded with 0
	align = ((((uintptr_t) prs->data) + expected_fields_size) & 0x7UL);
	padding = align ? 8 - align : 0;

	// The data returned in prv->data should only contain the common
	// tracepoint fields, the expected fields for this specific tracepoint,
	// and padding. The expected_fields_size accounts for the both common
	// and specific tracepoint fields.
	unknown = prs->size - expected_fields_size
			    - padding;

	if (unknown) {
		fprintf(stderr,
			"WARNING: tracepoint %s unknown bytes: %zd\n"
			" header.size             = %zd\n"
			" sizeof(header)          = %zd\n"
			" record extra            = %zd\n"
			" data size               = %zu\n"
			" expected tp fields size = %zu\n"
			" padding                 = %u\n"
			" record addr             = %p\n"
			" data addr               = %p\n",
			name, unknown,
			prs->header.size, sizeof(struct perf_event_header), record_extra,
			prs->size, expected_fields_size, padding,
			prs, prs->data);
	}
}

void report_lost_records(struct perf_lost_record *plr)
{
	fprintf(stderr, "\n\nrecords lost: id=%"PRIu64" lost=%"PRIu64"\n\n",
		plr->id, plr->lost);

}

void print_tracepoint(struct perf_record_sample *prs)
{
	struct tp_common *tpc;


	//int i;
	//printf("raw dump\n");
	//for (i = 0; i < prs->header.size; i++) {
	//	printf("%c",((char *)prs)[i]);
	//}
	//printf("\nend raw dump\n");


	printf("%"PRIu64" ", prs->time);

	tpc = (struct tp_common *) prs->data;
	printf("{common_type = %d, common_flags=%u, common_preempt_count=%u, common_pid=%d} ",
	       tpc->common_type, tpc->common_flags, tpc->common_preempt_count, tpc->common_pid);


	switch(tpc->common_type) {
	case TP_SCHED_SWITCH:
	{
		struct tp_sched_switch *tp;
		tp = (struct tp_sched_switch *) tpc;
		check_tp_record_size(prs, "sched_switch", sizeof(*tp));
		printf("{prev_comm=%.16s, prev_pid=%d, prev_prio=%d, prev_state=%ld,"
		       " next_comm=%.16s, next_pid=%d, next_prio=%d}\n",
		       tp->prev_comm, tp->prev_pid, tp->prev_prio, tp->prev_state,
		       tp->next_comm, tp->next_pid, tp->next_prio);
		break;
	}
	case TP_SCHED_WAKEUP:
	{
		struct tp_sched_wakeup *tp;
		tp = (struct tp_sched_wakeup *) tpc;
		check_tp_record_size(prs, "sched_wakeup", sizeof(*tp));
		printf("{comm=%.16s, pid=%d, prio=%d, success=%d target_cpu=%d}\n",
		       tp->comm, tp->pid, tp->prio, tp->success, tp->target_cpu);
		break;
	}
	default:
		fprintf(stderr, "Error: Unknown tracepoint type: %d\n",
			tpc->common_type);
		exit(EXIT_FAILURE);
	}
}

#define PERF_CHECK_MACRO(var, name)           \
	if (var & name) {                     \
		printf(#name " ");    \
		var &= ~name;                 \
	}

#define PERF_CHECK_REMAINING(var) var

void print_macros(uint32_t type, uint64_t sample_type, uint64_t read_format)
{
	printf("type (%"PRIu32") = ", type);
	PERF_CHECK_MACRO(type, PERF_TYPE_HARDWARE);
	PERF_CHECK_MACRO(type, PERF_TYPE_SOFTWARE);
	PERF_CHECK_MACRO(type, PERF_TYPE_TRACEPOINT);
	PERF_CHECK_MACRO(type, PERF_TYPE_HW_CACHE);
	PERF_CHECK_MACRO(type, PERF_TYPE_RAW);
	PERF_CHECK_MACRO(type, PERF_TYPE_BREAKPOINT);
	if (PERF_CHECK_REMAINING(type))
		printf("UNKNOWN");

	printf(" sample_type (%"PRIu64") = ", sample_type);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_IP);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_TID);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_TIME);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_ADDR);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_READ);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_CALLCHAIN);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_ID);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_CPU);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_PERIOD);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_STREAM_ID);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_RAW);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_BRANCH_STACK);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_REGS_USER);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_STACK_USER);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_WEIGHT);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_DATA_SRC);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_IDENTIFIER);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_TRANSACTION);
	PERF_CHECK_MACRO(sample_type, PERF_SAMPLE_REGS_INTR);
	if (PERF_CHECK_REMAINING(sample_type))
		printf("UNKNOWN");

	printf(" read_format (%"PRIu64") = ", read_format);
	PERF_CHECK_MACRO(read_format, PERF_FORMAT_TOTAL_TIME_ENABLED);
	PERF_CHECK_MACRO(read_format, PERF_FORMAT_TOTAL_TIME_RUNNING);
	PERF_CHECK_MACRO(read_format, PERF_FORMAT_ID);
	PERF_CHECK_MACRO(read_format, PERF_FORMAT_GROUP);
	if (PERF_CHECK_REMAINING(read_format))
		printf("UNKNOWN");

	printf("\n");
}

int main(int argc, char **argv)
{
	struct perf_event_mmap_page *mpage;
	struct perf_event_header *header;
	struct perf_event_attr pe;
	struct perf_record_sample *prs;
	struct perf_lost_record *plr;
	char *dmpage;
	long long count;
	size_t n, tsize, dsize, msize, dmask;
	size_t i;
	unsigned long long int hi, ti, ci;
	int cpu;
	pid_t pid;
	unsigned long flags;
	size_t nr_pages;
	char *event_copy;

	int ev_id[] = {TP_SCHED_SWITCH, TP_SCHED_WAKEUP};
	//int ev_id[] = {TP_SCHED_SWITCH};
	int nr_events = COUNT_OF(ev_id);
	int fds[nr_events];

	event_copy = malloc(PERF_SAMPLE_MAX_SIZE);
	if (!event_copy) {
		perror("Cannot allocate memory");
		exit(EXIT_FAILURE);
	}

	// Initialize common perf attribute argument
	memset(&pe, 0, sizeof(struct perf_event_attr));
	pe.type = PERF_TYPE_TRACEPOINT;
	//pe.config = <set below>
	pe.size = sizeof(struct perf_event_attr);
	pe.sample_period = 1; // record every n-events, setting to 1 records all
	pe.sample_type = PERF_SAMPLE_TIME | PERF_SAMPLE_RAW;
	pe.wakeup_events = 9999999;
	pe.disabled = 1;
	pe.use_clockid = 1;
	pe.clockid = CLOCK_MONOTONIC;

	// tracing other than the current thread requires CAP_SYS_ADMIN
	// capability of /proc/sys/kernel/perf_event_paranoid value less than 1.
	cpu = 0;
	pid = -1;
#ifdef PERF_FLAG_FD_CLOEXEC
	flags = PERF_FLAG_FD_CLOEXEC;
#else
	flags = 0;
#endif

// gdb perf report extract
//$1 = {type = 2, size = 112, config = 316, {sample_period = 1, sample_freq = 1}, sample_type = 1479, read_format = 4, disabled = 1, inherit = 1, pinned = 0, exclusive = 0, exclude_user = 0, 
//  exclude_kernel = 0, exclude_hv = 0, exclude_idle = 0, mmap = 1, comm = 1, freq = 0, inherit_stat = 0, enable_on_exec = 0, task = 1, watermark = 0, precise_ip = 0, mmap_data = 0, sample_id_all = 1,
//  exclude_host = 0, exclude_guest = 1, exclude_callchain_kernel = 0, exclude_callchain_user = 0, mmap2 = 1, comm_exec = 1, use_clockid = 0, context_switch = 0, write_backward = 0, namespaces = 0, 
//  ksymbol = 1, bpf_event = 1, __reserved_1 = 0, {wakeup_events = 0, wakeup_watermark = 0}, bp_type = 0, {bp_addr = 0, kprobe_func = 0, uprobe_path = 0, config1 = 0}, {bp_len = 0, kprobe_addr = 0,
//  probe_offset = 0, config2 = 0}, branch_sample_type = 0, sample_regs_user = 0, sample_stack_user = 0, clockid = 0, sample_regs_intr = 0, aux_watermark = 0, sample_max_stack = 0, __reserved_2 = 0}

// strace perf record extract
//   ...
//   perf_event_open(0x1494938, -1, 0, -1, 0x8 /* PERF_FLAG_??? */) = 4
//   ... // a lot of perf_event_open
//   perf_event_open(0x1494d28, -1, 0, -1, 0x8 /* PERF_FLAG_??? */) = 61
//   ... // a lot of perf_event_open
//   perf_event_open(0x149bcb8, -1, 0, -1, 0x8 /* PERF_FLAG_??? */) = 117
//   ... // a lot of perf_event_open

//   mmap(NULL, 528384, PROT_READ|PROT_WRITE, MAP_SHARED, 4, 0) = 0x7f501e160000
//   fcntl(4, F_SETFL, O_RDONLY|O_NONBLOCK)  = 0
//   ioctl(4, PERF_EVENT_IOC_ID, 0x7ffc29af29a8) = 0

//   ioctl(61, PERF_EVENT_IOC_SET_OUTPUT, 0x4) = 0
//   fcntl(61, F_SETFL, O_RDONLY|O_NONBLOCK) = 0
//   ioctl(61, PERF_EVENT_IOC_ID, 0x7ffc29af29a8) = 0
//   ioctl(117, PERF_EVENT_IOC_SET_OUTPUT, 0x4) = 0
//   fcntl(117, F_SETFL, O_RDONLY|O_NONBLOCK) = 0
//   ioctl(117, PERF_EVENT_IOC_ID, 0x7ffc29af29a8) = 0


	// open events
	for (i = 0; i < nr_events; i++) {
		pe.config = ev_id[i];
		fds[i] = perf_event_open(&pe, pid, cpu, -1, flags);
		if (fds[i] == -1) {
			perror("Error calling perf_event_open on grouped event");
			exit(EXIT_FAILURE);
		}
	}

	nr_pages = 2; // perf buffer is allocated at mmap() time
	msize = PAGE_SIZE;
	dsize = nr_pages * PAGE_SIZE;
	tsize = msize + dsize;
	dmask = dsize - 1;

	mpage = mmap(NULL, tsize, PROT_READ | PROT_WRITE,
		    MAP_SHARED, fds[0], 0);
	if (mpage == MAP_FAILED) {
		perror("Error mapping perf pages 2");
		exit(EXIT_FAILURE);
	}

	if (fcntl(fds[0], F_SETFL, O_RDONLY | O_NONBLOCK)) {
		perror("Error changing file desciptor properties");
		exit(EXIT_FAILURE);
	}

	for (i = 1; i < nr_events; i++) {
		// this MUST be done after mmap!
		if (ioctl(fds[i], PERF_EVENT_IOC_SET_OUTPUT, fds[0]) == -1) {
			perror("Error redirecting event file descriptor");
			exit(EXIT_FAILURE);
		}
		if (fcntl(fds[i], F_SETFL, O_RDONLY | O_NONBLOCK)) {
			perror("Error changing file desciptor properties");
			exit(EXIT_FAILURE);
		}
	}

	// enable events
	for (i = 0; i < nr_events; i++)
		ioctl(fds[i], PERF_EVENT_IOC_ENABLE, 0);

	dmpage = ((char *)mpage) + PAGE_SIZE;
	assert(mpage->data_offset == PAGE_SIZE);

	hi = mpage->data_head;
	ti = mpage->data_tail;
	io_uring_smp_rmb();
	ci = ti;
	cnt = 0;
	printf("data_offset = %lx data_size = %lx\n",
	       mpage->data_offset, mpage->data_size);

	while (1) {

		hi = mpage->data_head;
		io_uring_rmb();

		if (hi == ci)
			continue;

		// The record might be split between the beginning and the end
		// of the perf buffer.
		//
		// It's not possible to create a "magic ring buffer" as mmaping
		// the perf buffer with an offset is not allowed. Therefore the
		// only way is to copy the struture in an intermediate buffer
		//
		// However, the header cannot be split because it's 64 bit
		// aligned and 64 bit long. Hence, it's safe to access it.
		header = (struct perf_event_header *) (dmpage + (ci & dmask));

		// adapted from perf tools/perf/util/mmap.c:perf_mmap__read()
		if (((ci & dmask) + header->size) != ((ci + header->size) & dmask)) {
			unsigned int offset = ci;
			unsigned int len = header->size;
			unsigned int cpy;
			void *dst = event_copy;

			do {
				cpy = min(dsize - (offset & dmask), len);
				memcpy(dst, dmpage + (offset & dmask), cpy);
				offset += cpy;
				dst += cpy;
				len -= cpy;
			} while (len);

			// TODO where the fork is union perf_event defined?
			//event = (union perf_event *)map->event_copy;
			header = (struct perf_event_header *) event_copy;
		}

		// process record
		switch(header->type) {
		case PERF_RECORD_SAMPLE:
			prs = (struct perf_record_sample *) header;
			check_record_sample_size(prs);
			print_tracepoint(prs);
			break;
		case PERF_RECORD_LOST:
			plr = (struct perf_lost_record *) header;
			report_lost_records(plr);
			break;
		default:
			fprintf(stderr, "Error: Unknown record: %d\n",
				header->type);
			perf_print_header_type(header);
			exit(EXIT_FAILURE);
		}

		// advance buffer
		ci += header->size;
		mpage->data_tail = ci;
		cnt++;
		io_uring_wmb();
	}

	munmap(mpage, tsize);
}

