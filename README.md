Read Linux Kernel tracepoints with the perf\_event\_open system call Example
============================================================================

This example enables and outputs to stdout two tracepoints only for cpu 0.

Currently, it only supports the "sched\_switch" and "sched\_wakeup" events.
Also, you might need to manually update the id's of this events. To do so you
need to update the following macros:

```
#define TP_SCHED_SWITCH 318
#define TP_SCHED_WAKEUP 320
```

The events and their Id's are exposed by the kernel. For instance, the
"sched\_switch" event Id is found under:

```
cat /sys/kernel/debug/tracing/events/sched/sched_switch/id
```

You can select which events to run by adding or removing events from the
following variable:

```
	int ev_id[] = {TP_SCHED_SWITCH, TP_SCHED_WAKEUP};
```

In order to work for other events, you also need to provide a definition of the
event similar to:

```
struct __attribute__((__packed__)) tp_sched_switch {
```

Yes, all of this could be automated by parsing the files under
`/sys/kernel/debug/tracing/events` but we already have `perf` for that ;)

Hope it helps! :D
